\section{Linear independence and bases}

In this section, we consider problems related to finding the smallest
set that spans a particular vector space.  These problems arise in
many applications from network design to big data.

\subsection{Linear dependence}
Intuitively, a set of vectors $\uv_1,\uv_2,\ldots,\uv_k$ is linearly
dependent if some vector in the set can be written as a linear
combination of the other vectors.  Formaly, we say that
$\uv_1,\uv_2,\ldots,\uv_k$ is {\em linearly dependent} if there exist
$\alpha_1,\alpha_2,\ldots,\alpha_k$ not all equal to zero such that
\[
0 = \alpha_1\uv_1 + \alpha_2\uv_2 + \cdots + \alpha_k\uv_k.
\]
To see that this is equivalent to the first definition, assume, for
example that we can write $\uv_1 = \alpha_2\uv_2 + \cdots + \alpha_k\uv_k$.  This implies that $(-1)\uv_1 + \alpha_2\uv_2 + \cdots+ \alpha_k\uv_k = 0$, and note that the coefficient of $\uv_1$ is not zero.

On the other hand, we say that $\uv_1,\uv_2,\ldots,\uv_k$ is {\em
  linearly independent} if we can write
\[
0 = \alpha_1\uv_1 + \alpha_2\uv_2 + \cdots + \alpha_k\uv_k.
\]
only when $\alpha_1=\alpha_2=\cdots=\alpha_k=0$.

\subsection{Removing vectors}

Consider a vector space $\V=\vspan~\{\uv_1,\uv_2,\ldots,\uv_k\}$.
What are the conditions allowing us to remove some vector in the set
while keeping the span unchanged?  Recall that in $\rf^3$, when we
have two vectors $\uv,\vv$ whose span form a plane, adding another
vector $\vect{w}$ belonging to the plane does not increase the span.
This is because $\vect{w}\in\vspan~\{\uv,\vv\}$.  We can generalize
this idea to have the following lemma.

\begin{lemma}[Superfluous vector]
  \label{lemma:superfluous}
  For a set of vectors $S$ and vector $\vv\in S$, if $\vv$ can be
  written as a linear combination of other vectors in $S$, then
  \[
  \vspan~S = \vspan~(S-\{\vv\}).
  \]
\end{lemma}
\begin{proof}
  Assume that $S=\{\vv,\uv_1,\uv_2,\ldots,\uv_k\}$.  Since $\vv$ can
  be written as a linear combination of other vectors, there exist
  $\alpha_1,\alpha_2,\ldots,\alpha_k$ such that
  \[
  \vv = \alpha_1\uv_1+\alpha_2\uv_2+\cdots+\alpha_k\uv_k.
  \]
  Consider any vector $\vect{w}\in\vspan~S$; thus, we can write
  \[
  \vect{w} = \beta_0\vv + \beta_1\uv_1 + \beta_2\uv_2 +\cdots+\beta_k\uv_k.
  \]
  Plugging in $\vv$, we get that
  \begin{align*}
    \vect{w} & =  \beta_0\left(\alpha_1\uv_1+\cdots+\alpha_k\uv_k\right) + \beta_1\uv_1 + \beta_2\uv_2 +\cdots+\beta_k\uv_k \\
    &= (\beta_0\alpha_1+\beta_1)\uv_1 +
    (\beta_0\alpha_2+\beta_2)\uv_2 +
    \cdots+(\beta_0\alpha_k+\beta_k)\uv_k,
  \end{align*}
  implying that $\vect{w}\in\vspan~(S-\{\vv\})$.
\end{proof}

\subsection{The Basis}
We say that $\vect{g}_1,\vect{g}_2,\ldots,\vect{g}_k$ are {\em
  generators} for vector space $\V$ if
$\vspan~\{\vect{g}_1,\vect{g}_2,\ldots,\vect{g}_k\} = \V$.  A set of
linearly independent generators for vector space $\V$ are called a
{\em basis} of vector space $\V$.  (Use {\em bases} as its plural.)

\begin{lemma}[Unique representation]
  Let $\uv_1,\uv_2,\ldots,\uv_k$ be a basis for vector space $\V$.
  For any $\vv\in\V$, there is a unique way to write $\vv$ as a linear
  combination of $\uv_1,\ldots,\uv_k$.
\end{lemma}
\begin{proof}
  We prove by contradiction.  Assume that there exists a vector
  $\vv\in\V$ with more than one ways to be written as linear
  combinations of the basis.  Thus, there exist
  \[ \alpha_1,\alpha_2,\ldots,\alpha_k, \] and
  \[ \beta_1,\beta_2,\ldots,\beta_k, \]
  that are not equal (i.e., there exists $i$ where
  $\alpha_i\neq\beta_i$)
  such that
  $\vv=\alpha_1\uv_1+\alpha_2\uv_2+\cdots+\alpha_k\uv_k$ and
  $\vv=\beta_1\uv_1+\beta_2\uv_2+\cdots+\beta_k\uv_k$.  This implies that
  \[
  \alpha_1\uv_1+\alpha_2\uv_2+\cdots+\alpha_k\uv_k=\vv=\beta_1\uv_1+\beta\uv_2+\cdots+\beta_k\uv_k,
  \]
  and
  \[
  (\alpha_1-\beta_1)\uv_1 +
  (\alpha_2-\beta_2)\uv_2 + \cdots + (\alpha_k-\beta_k)\uv_k=0.
  \]
  Since $\alpha_i\neq\beta_i$, we have that at least one of the
  coefficients is non-zero, implying that $\uv_1,\ldots,\uv_k$ are not
  linearly independent.  This contradicts the assumption that
  $\uv_1,\ldots,\uv_k$ form a basis.
\end{proof}

\begin{lemma}
  A subset of a linearly indendent set of vectors is also linearly
  independent.  More specifically, if $A$ is linearly independent,
  $A-\{\vv\}$ is also linearly independent.
  \label{lem:subset-lin-indep}
\end{lemma}
\begin{proof}
  We can prove by contraposition.  It is easy to see that if
  $A-\{\vv\}$ is linearly dependent, $A$ is also linearly dependent.
\end{proof}

\subsection{Finding bases}

\subsubsection{Grow and Shrink algorithms}

There are two ``directions'' for finding bases.

{\bf Growing.}  We maintain a set of linearly independent vectors
(starting with an empty set) and keep adding more vectors into this
set until it spans the vector space.

\vspace{0.1in}
\noindent
\fbox{\parbox{\textwidth}{
    {\bf Algorithm} Grow($\V$) \\
    $S\leftarrow\emptyset$ \\
    Repeat while possible, \\
    --- find vector $\vv \in\V$ such that $\vv\not\in\vspan~S$ \\
    --- $S\leftarrow S\cup\{\vv\}$
}}
\vspace{0.05in}

{\bf Shrinking.}  We start with a set of vectors that span the vector
space, and keep removing vectors until the set is linearly
independent.

\vspace{0.1in}
\noindent
\fbox{\parbox{\textwidth}{
    {\bf Algorithm} Shrink($\V, S$)  // assume that $S$ span $\V$\\
    Repeat while possible, \\
    --- find vector $\vv \in S$ such that $\vspan~(S-\{\vv\}) = \vspan~S$ \\
    --- $S\leftarrow S-\{\vv\}$
}}
\vspace{0.05in}

\subsubsection{Partial correctness of the Grow algorithm}

We consider the Grow algorithm.  Assume that the algorithm terminates,
the while loop guarantees that there is no such vector
$\vv\in{\V}$ not in the span of $S$; thus, $S$ is the set of
generators for $\V$.  It remains to check if $S$ is linearly
independent.

To prove this fact, we first prove a fairly useful lemma.

\begin{lemma}
  Consider set of vectors $A=\{\uv_1,\uv_2,\ldots,\uv_k\}$ and vector
  $\vv$.  Vector $vv\in\vspan~A$ if and only if it is possible to
  write
  \[
  0 = \beta\vv + \alpha_1\uv_1 + \cdots + \alpha_k\uv_k,
  \]
  such that $\beta\neq 0$.
  \label{lem:non-zero-coeff-span}
\end{lemma}
\begin{proof} We first prove the ``if'' direction.  Since there exist
  $\beta,\alpha_1,\ldots,\alpha_k$ such that $\beta\neq 0$ and
  \[
  0 = \beta\vv + \alpha_1\uv_1 + \cdots + \alpha_k\uv_k,
  \]
  We have that
  \[
  \vv = (-\alpha_1/\beta)\uv_1 + (-\alpha_2/\beta)\uv_2 + \cdots +
  (-\alpha_k/\beta)\uv_k.
  \]
  (Note that this requires that $\beta\neq 0$.)  This shows that $\vv$
  is a linear combination of $\uv_1,\ldots,\uv_k$, i.e.,
  $\vv\in\vspan~A$.

  To prove the ``only-if'' direction, assume that $\vv\in\vspan~A$;
  thus, we can write
  \[
  \vv = \alpha_1\uv_1 + \alpha_2\uv_2 + \cdots + \alpha_k\uv_k,
  \]
  implying that
  \[
  0 = (-1)\vv + \alpha_1\uv_1 + \alpha_2\uv_2 + \cdots + \alpha_k\uv_k,
  \]
  as required.
\end{proof}

The next lemma that implies that the output $S$ of the Grow algorithm
is linearly independent.

\begin{lemma}
  Suppose that set of vectors $A$ is linearly independent.  For any
  $\vv\not\in\vspan~A$, $A\cup\{\vv\}$ is also linearly independent.
\end{lemma}
\begin{proof}
  We prove by contraposition.  Assume that $A\cup\{\vv\}$ is linearly
  dependent.

  Let $A=\{\uv_1,\ldots,\uv_k\}$.  By the definition of linear
  dependence, we have that there exist
  $\alpha_0,\alpha_1,\ldots,\alpha_k$ such that
  \[
  0 = \alpha_0\vv + \alpha_1\uv_1 + \cdots + \alpha_k\uv_k,
  \]
  and not all $\alpha_i$ are zero.  Observe that if $\alpha_0=0$, we
  have that
  \[
  0 = \alpha_1\uv_1 + \cdots + \alpha_k\uv_k,
  \]
  and $\alpha_i\neq 0$ for some $1\leq i\leq k$, implying that $A$ is
  not linearly independent.  Since this contradicts the assumption on
  $A$, we know that $\alpha_0\neq 0$.

  It follows from Lemma~\ref{lem:non-zero-coeff-span} that
  $\vv\in\vspan~A$, as required.
\end{proof}

\subsubsection{Partial correctness of the Shrink algorithm}

\begin{lemma}
  After termination, the output of the Shrink algorithm is linearly
  independent.
\end{lemma}
\begin{proof}
  We prove by contradiction.  Let $S=\{\uv_1,\ldots,\uv_k\}$.  Suppose
  that the output $S$ is linearly dependent.  This implies that we can
  write
  \[
  0 = \alpha_1\uv_1 + \alpha_2\uv_2 + \cdots + \alpha_k\uv_k,
  \]
  such that at least one coefficient is non-zero.  Assume that
  $\alpha_i\neq 0$.  This implies that
  \[
  \uv_i = (-\alpha_1/\alpha_i)\uv_1 + \cdots +
  (-\alpha_{i-1}/\alpha_i)\uv_{i-1} + 
  (-\alpha_{i+1}/\alpha_i)\uv_{i+1} + \cdots +
  (-\alpha_k/\alpha_i)\uv_k.
  \]
  Thus $\uv_i\in\vspan~(S-\{\uv_i\})$.  This means that the algorithm
  can still proceed in the while loop and contradicts the fact the
  algorithm terminates.
\end{proof}

\subsubsection{Why we are not done}

A few issues remain.

\begin{itemize}
\item For the Grow algorithm, we only show that the algorithm is
  correct if it terminates.  However, we have not shown that it will
  definitely terminate. (Note that there are vector spaces, in
  general, that the Grow algorithm will not terminate.  But it will
  terminate on vector spaces as defined in this course.)
\item For the Shrink algoithm, we have not address how to find the
  initial set of vectors that span the space.
\end{itemize}

We will address these issues in the next subsection.

\subsection{Dimensions} 

Naturally, when we look at vector spaces $\rf^1, \rf^2,$ and $\rf^3$,
we can think of the number of ``independent'' directions that we can
move in these spaces.  Intuitively, for $\rf^1$ we can see that there
are only 1 direction, for $\rf^2$ there are 2 directions, and for
$\rf^3$, there are 3 directions.  These ideas are captured by the
concept of ``dimensions,'' when we say that vector space $\rf^1$ is
1-dimensional, $\rf^2$ is 2-dimensional, and finally $\rf^3$ is
3-dimensional.  As you see from the previous subsection, each vector
in a basis represents one particular ``direction'' in the vector
space.  Therefore, it is natural to define {\em the dimension of a
  vector space $\V$}, denoted by $\dim \V$ as the
number of vectors in a basis.

Since there can be many bases for a vector space, this definition
looks suspicious, because it might be possible that a vector space may
have many bases with different sizes.

The first major result for this course is the Basis Theorem that
states that every basis has the same size.

\subsubsection{The Basis Theorem}

\begin{lemma}[Exchange lemma]
  Consider a set of vectors $S$, vector $\vect{z}\in\vspan~S$, and
  subset $A\subseteq S$.  Suppose that $A\cup\{\vect{z}\}$ is linearly
  independent.  There exists $\vect{w}\in S$ such that
  $\vspan~S=\vspan~(S\cup\{\vect{z}\}-\{\vect{w}\})$.
  \label{lem:exchange}
\end{lemma}
\begin{proof}
  Assume that $A=\{\vv_1,\vv_2,\ldots,\vv_m\}$ and
  $S=\{\vv_1,\vv_2,\ldots,\vv_m,\uv_1,\uv_2,\ldots,\uv_k\}$.  Since
  $\vect{z}\in\vspan~S$, we can write
  \[
  \vect{z} =
  \alpha_1\vv_1+\alpha_2\vv_2+\cdots+\alpha_m\vv_m+
  \alpha'_1\uv_1+\alpha'_2\uv_2+\cdots+\alpha'_k\uv_k.
  \]
  Subtracting $\vect{z}$ on both sides, we get
  \begin{equation}
    \label{eqn:exchange-z}
    0 = (-1)\cdot\vect{z} +
    \alpha_1\vv_1+\alpha_2\vv_2+\cdots+\alpha_m\vv_m+
    \alpha'_1\uv_1+\alpha'_2\uv_2+\cdots+\alpha'_k\uv_k.
  \end{equation}
  Note that $\alpha'_1,\alpha'_2,\ldots,\alpha'_k$ cannot all be zero
  because this would imply that
  \[
  0 = (-1)\cdot\vect{z} +
  \alpha_1\vv_1+\alpha_2\vv_2+\cdots+\alpha_m\vv_m,
  \]
  which contradicts the fact that $A\cup\{\vect{z}\}$ is linearly
  independent.  Hence, let $i$ be such that $\alpha'_i\neq 0$.  We can
  rewrite~(\ref{eqn:exchange-z}) as
  \[
  -\alpha'_i\uv_i = (-1)\cdot\vect{z} +
  \alpha_1\vv_1+\cdots+\alpha_m\vv_m+
  \alpha'_1\uv_1+\cdots+\alpha'_{i-1}\uv_{i-1}+\alpha'_{i+1}\uv_{i+1}+\cdots+\alpha'_k\uv_k.
  \]
  Thus,
  \begin{align*}
  \uv_i = & (1/\alpha'_i)\cdot\vect{z} +
  (-\alpha_1/\alpha'_i)\vv_1+\cdots+
  (-\alpha_m/\alpha'_i)\vv_m+ \\
  & (-\alpha'_1/\alpha'_i)\uv_1+\cdots+
  (-\alpha'_{i-1}/\alpha'_i)\uv_{i-1}+
  (-\alpha'_{i+1}/\alpha'_i)\uv_{i+1}+\cdots+
  (-\alpha'_k/\alpha'_i)\uv_k,
  \end{align*}
  implying that $\uv_i\in\vspan~(S\cup\{\vect{z}\}-\{\vect{\uv_i}\})$.

  Since $\vect{z}\in\vspan~S$, we have
  \[
  \vspan~S=\vspan~(S\cup\{\vect{z}\}).
  \]
  Furthermore, since
  $\uv_i\in\vspan~(S\cup\{\vect{z}\}-\{\vect{\uv_i}\})$, by the
  Superfluous Vector Lemma~\ref{lemma:superfluous}, we have that
  \[
  \vspan~S=\vspan~(S\cup\{\vect{z}\})=\vspan~(S\cup\{\vect{z}\}-\{\vect{\uv_i}\})
  \]
  Therefore, we can let $\uv_i$ to be vector $\vect{w}$ as required.
\end{proof}

\begin{lemma}[Morphing lemma]
  Let $S$ be a set of vectors that spans vector space $\V$.
  Suppose that $R$ be a set of linearly independent vectors in
  $\V$.  We have that $|R|\leq|S|$.
\end{lemma}

The Morphing lemma is crucial for proving the Basis Theorem which is
very important in linear algebra.

\begin{theorem}[Basis Theorem]
  If $A$ and $B$ are bases of vector space $\V$, $|A|=|B|$.
\end{theorem}
\begin{proof}
  Note that $A$ is linearly independent and $B$ spans $\V$;
  the Morphing lemma implies that $|A|\leq|B|$.  On the other hand, to
  see that $|B|\leq|A|$, note that $B$ is linearly independent and $A$
  spans $\V$.
\end{proof}

Let's see the proof the Morphing lemma.

\begin{proof}[Proof (Morphing Lemma)]
  Let $R=\{\uv_1,\uv_2,\ldots,\uv_k\}$.  We show that we can construct
  $S_0,S_1,\ldots,S_k$ such that for $0\leq i\leq k$, (i)
  $\{\uv_1,\ldots,\uv_i\}\subseteq S_i$, (ii) $|S_i| = |S|$, and (iii)
  $\vspan~S_i = \vspan~S$.  To see that this implies the lemma note
  that $S_k\supseteq R$; thus,
  \[
  |R|\leq |S_k| = |S|,
  \]
  as required.

  We construct $S_i$'s by induction.  For the basic step, we let
  $S_0=S$.  Conditions (i) - (iii) can be easily verified for this
  case.

  For any $0\leq i<k$, assume that we can construct $S_i$, we show how
  to construct $S_{i+1}$.  Let $A=\{\uv_1,\ldots,\uv_i\}$.  Since $R$
  is linearly independent, $A\cup\{\uv_{i+1}\}$ is also linear
  independent, because $A\cup\{\uv_{i+1}\}\subseteq R$
  (from Lemma~\ref{lem:subset-lin-indep}).

  We apply the Exchange Lemma (Lemma~\ref{lem:exchange}) to $S_i$ with
  $\vect{z}=\uv_{i+1}$.  This implies that there exists $\vect{w}\in
  S_i$ such that $\vect{w}\not\in A$ and $\vspan~S_i =
  \vspan~(S_i\cup\{\vect{z}\}-\{\vect{w}\})$.

  Thus, we can let
  \[
  S_{i+1}=S_i\cup\{\vect{\uv_{i+1}}\}-\{\vect{w}\}.
  \]
  We know from the Exchange Lemma that (iii) holds.  More over, we
  also have (i) $\{\uv_1,\ldots,\uv_{i+1}\}\subseteq S_{i+1}$ and (ii)
  $|S_{i+1}|=|S_i|=|S|$, as needed.
\end{proof}

In this course, we deal with only vector spaces whose dimensions are
finite such as $\rf^n, \cf^n$, or $GF(2)^n$.  We call them {\em finite
  dimension vector spaces}.

\subsection{Other useful facts}

\subsection{Correctness of the Grow and Shrink algorithms}

Consider a vector space $\ff^n$.  Let denote by $\vect{e}_i$ a vector
over field $\ff$ whose elements are 0 except the one at position $i$
being 1, i.e.,
\[
\vect{e}_i = [\underbrace{0,0,\ldots}_\text{$i-1$},1,\underbrace{\ldots,0}_\text{$n-i$}].
\]
Note that a set $B=\{\vect{e}_1, \vect{e}_2,\ldots, \vect{e}_n\}$ is
linearly independent and spans $\ff^n$; hence it is a basis.
Moreover, this implies that $\ff^n$ is an $n$-dimensional vector
space.

We are ready to prove the termination of the Grow algorithm if it runs
on a finite-dimensional vector space $\V$.

\begin{theorem}
  The Grow algorithm on a finite-dimensional vector space $\V$
  terminates.
\end{theorem}
\begin{proof}
  TODO
\end{proof}

Any set of linear independent vectors can be extended to get a basis.

\begin{lemma}
  For a finite dimension vector space $\V$ for a set of linearly
  independent vectors $A$ in $\V$, there is a basis $B$ such that
  $A\subseteq B$.
  \label{lemma:basis-extension}
\end{lemma}
\begin{proof}[Proof (sketch)]
  We can runs the modified Grow algorithm.  Let $k=|A|$. We start with
  empty set $S$ and keep adding vectors from $A$ in the first $k$
  steps.  Then we continue the algorithm to its termination to get
  basis $B$.  Clearly, $A\subseteq B$.
\end{proof}

\subsection{Testing span}
\label{sect:testing-span}

Given a set of $n$-vectors $S=\{\uv_1,\uv_2,\ldots,\uv_k\}$ over $\ff$
and an $n$-vector $\vv$, can we check if $\vv\in\vspan~S$?

Consider the following concrete example from the exercise.  Consider
$3$-vectors over $\rf$.  Let $\uv_1=[1,2,3], \uv_2=[1,1,1],
\uv_3=[1,2,2]$.  We would like to check if
$\vv=[10,13,29]\in\vspan~\{\uv_1,\uv_2,\uv_3\}$.

Let us define variables $\alpha_1,\alpha_2,\alpha_3$ such that
$\vv=\alpha_1\uv_1 + \alpha_2\uv_2 + \alpha_3\uv_3$; i.e., we want
$\alpha_1,\alpha_2,\alpha_3$ to be such that
\[
\alpha_1
\begin{bmatrix}
  1 \\
  2 \\
  3
\end{bmatrix}
+
\alpha_2
\begin{bmatrix}
  1 \\
  1 \\
  1
\end{bmatrix}
+
\alpha_3
\begin{bmatrix}
  1 \\
  2 \\
  2
\end{bmatrix}
=
\begin{bmatrix}
  10 \\
  13 \\
  29
\end{bmatrix}
\]

We can write these constraints down as the following linear equations.
\[
\begin{array}{ccccccl}
  1\alpha_1 &+& 1\alpha_2 &+& 1\alpha_3 &=& 10\\
  2\alpha_1 &+& 1\alpha_2 &+& 2\alpha_3 &=& 13\\
  3\alpha_1 &+& 1\alpha_2 &+& 2\alpha_3 &=& 29\\
\end{array}
\]

We also can, equivalently, write them in matrix form.
\[
\begin{bmatrix}
  1 & 1 & 1 \\
  2 & 1 & 2 \\
  3 & 1 & 2
\end{bmatrix}
\begin{bmatrix}
  \alpha_1 \\
  \alpha_2 \\
  \alpha_3
\end{bmatrix}
=
\begin{bmatrix}
  10 \\
  13 \\
  29
\end{bmatrix}
\]

To find $\alpha_i$'s, we can use Gaussian Elimination.  First, we
multiply the first equation by 2 and 3 and subtract the results from
the second and the third lines to obtain:

\[
\begin{bmatrix}
  1 & 1 & 1 \\
  0 & -1 & 0 \\
  0 & -2 & -1
\end{bmatrix}
\begin{bmatrix}
  \alpha_1 \\
  \alpha_2 \\
  \alpha_3
\end{bmatrix}
=
\begin{bmatrix}
  10 \\
  -7 \\
  -1
\end{bmatrix}
\]

We then multiply the second row by $-1$.  After that we multiply it by
2 and add the result from the third row to obtain:

\[
\begin{bmatrix}
  1 & 1 & 1 \\
  0 & 1 & 0 \\
  0 & 0 & -1
\end{bmatrix}
\begin{bmatrix}
  \alpha_1 \\
  \alpha_2 \\
  \alpha_3
\end{bmatrix}
=
\begin{bmatrix}
  10 \\
  7 \\
  13
\end{bmatrix}
\]

Multiply the third row by $-1$ we have a much simpler system to solve
because the coefficients form the upper-right triangle of the matrix:

\[
\begin{bmatrix}
  1 & 1 & 1 \\
  0 & 1 & 0 \\
  0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
  \alpha_1 \\
  \alpha_2 \\
  \alpha_3
\end{bmatrix}
=
\begin{bmatrix}
  10 \\
  7 \\
  -13
\end{bmatrix}
\]

We finally see that $\alpha_1=16, \alpha_2=7,$ and $\alpha_3=-13$.
